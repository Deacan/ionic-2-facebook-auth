import { IProfileResponse } from './../interfaces/index';
import { Config } from './../../config';
import { Http } from '@angular/http';
import { IOAuthProvider } from './../interfaces/oauth-provider.interface';
import { Injectable } from '@angular/core';
import { Oauth } from 'ng2-cordova-oauth/core';
import { Facebook } from 'ng2-cordova-oauth/provider/facebook';
import { ILoginResponse, IOAuthProfile } from '../interfaces/index';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';


@Injectable()
export class FacebookOauthProvider implements IOAuthProvider {


  private _cordovaOauth: Oauth;
  private _facebook: Facebook;

  //#region getter/setter:
  public get facebook(): Facebook {
    return this._facebook;
  }

  public set facebook(v: Facebook) {
    this._facebook = v;
  }

  public get cordovaOauth(): Oauth {
    return this._cordovaOauth;
  }

  public set cordovaOauth(v: Oauth) {
    this._cordovaOauth = v;
  }
  //#endregion

  constructor(private _http: Http, private _config: Config) {
    this.facebook = new Facebook({
      clientId: this._config.facebook.appId,
      appScope: this._config.facebook.scope
    });
    this.cordovaOauth = new Oauth();
  }


  login(): Observable<any> {
    return Observable.fromPromise(
      this.cordovaOauth.login(this.facebook)
        .then((x: ILoginResponse) =>
          x.access_token
        )
    );

  }
  getProfile(accessToken: string): Observable<IOAuthProfile> {
    let query = `access_token=${accessToken}&format=json`;
    let url = `${this._config.facebook.apiUrl}me?${query}`;

    return this._http.get(url)
      .map(x => x.json())
      .map((x: IProfileResponse) => {
        let profile: IOAuthProfile = {
          firstName: x.first_name,
          lastName: x.last_name,
          email: x.email,
          provider: 'facebook'
        };
        return profile;
      });
  }
}
