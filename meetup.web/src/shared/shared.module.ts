import { NgModule, ModuleWithProviders } from '@angular/core';
import { OAuthModule, ServiceModule } from './index';


@NgModule({
  imports: [
    OAuthModule,
    ServiceModule
  ],
  exports: []
})
export class SharedModule {
  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: []
    };
  }
}
