import { Injectable } from '@angular/core';

@Injectable()
export class Config {
  public wordpressApiUrl = 'http://demo.titaniumtemplates.com/wordpress/?json=1';
  public facebook = {
    apiUrl: 'https://graph.facebook.com/v2.3',
    appId: '1573434136306283',
    scope: ['email', 'public_profile']
  };
  public google = {
    apiUrl: 'https://www.googleapis.com/oauth2/v3',
    appId: '',
    scope: ['email']
  };
}
