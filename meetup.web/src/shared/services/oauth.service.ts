import { Observable } from 'rxjs/Observable';
import { OAuthProvidersListPage } from './../OAuth/list/oath-providers-list';
import { Injectable, Injector } from '@angular/core';
import { IOAuthProvider, IOAuthToken } from '../interfaces/index';
import { FacebookOauthProvider, GoogleOAuthProvider } from './index';

@Injectable()
export class OAuthService {

  private _oauthTokenKey: string = 'oauthToken';

  //#region getter/setters
  public get oauthTokenKey(): string {
    return this._oauthTokenKey;
  }

  public set oauthTokenKey(v: string) {
    this._oauthTokenKey = v;
  }
  //#endregion

  constructor(private _injector: Injector) { }

  public login(source: string): Observable<any> {

    return this.getOAuthService(source).login().map(
      accessToken => {
        if (!accessToken) {
          return Observable.throw('No access token found');
        }

        let oauthToken: IOAuthToken = {
          accessToken: accessToken,
          source: source
        };

        this.setOAuthToken(oauthToken);
        return oauthToken;
      }
    );
  }


  getProfile(): Observable<any> {
    if (!this.isAuthorized()) {
      return Observable.throw('You are not authorized');
    }

    let oauthService = this.getOAuthService();
    return oauthService.getProfile(this.getOAuthToken().accessToken);
  }

  private isAuthorized(): boolean {
    return !!this.getOAuthToken();
  }

  private getOAuthService(source?: string): IOAuthProvider {
    source = source || this.getOAuthToken().source;

    switch (source) {
      case 'facebook':
        return this._injector.get(FacebookOauthProvider);

      case 'google':
        return this._injector.get(GoogleOAuthProvider);
      default:
        throw new Error(`Source '${source}' is not valid`);
    }
  }

  private setOAuthToken(token: IOAuthToken) {
    localStorage.setItem(this._oauthTokenKey, JSON.stringify(token));
  }

  private getOAuthToken(): IOAuthToken {
    let token = localStorage.getItem(this._oauthTokenKey);
    return token ? JSON.parse(token) : null;
  }
}
