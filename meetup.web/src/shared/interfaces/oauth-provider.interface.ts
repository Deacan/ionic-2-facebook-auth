import { IOAuthProfile } from './index';

import { Observable } from 'rxjs/Observable';

export interface IOAuthProvider {
  login(): Observable<any>;
  getProfile(accessToken: string): Observable<IOAuthProfile>;
}
