import { IonicModule } from 'ionic-angular';
import { NgModule } from '@angular/core';
import { OAuthProvidersListPage } from './list/index';
import { OAuthProfileComponent } from './profile/index';


@NgModule({
  imports: [
    IonicModule
  ],
  exports: [],
  declarations: [
    OAuthProvidersListPage,
    OAuthProfileComponent
  ],
  entryComponents: [
    OAuthProvidersListPage,
    OAuthProfileComponent
  ]
})
export class OAuthModule { }
