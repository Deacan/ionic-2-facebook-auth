import { Config } from './../../config';
import { Http } from '@angular/http';
import { IOAuthProvider, ILoginResponse } from './../interfaces/index';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/fromPromise';
import { Google, Oauth } from 'ng2-cordova-oauth/core';


@Injectable()
export class GoogleOAuthProvider implements IOAuthProvider {

  private _cordovaOauth: Oauth;
  private _google: Google;

  public get cordovaOauth() {
    return this._cordovaOauth;
  }

  public set cordovaOauth(value: Oauth) {
    this._cordovaOauth = value;
  }

  public get google() {
    return this._google;
  }

  public set google(value: Google) {
    this._google = value;
  }

  constructor(private _http: Http, private _config: Config) {
    this.google = new Google({
      clientId: this._config.google.appId,
      appScope: this._config.google.scope
    });
    this.cordovaOauth = new Oauth();
  }

  login(): Observable<any> {
    return Observable.fromPromise(this.cordovaOauth.login(this.google).then((x: ILoginResponse) => x.access_token));
  }

  getProfile(accessToken: string): Observable<any> {
    let query = `access_token=${accessToken}`;
    let url = `${this._config.google.apiUrl}userinfo?${query}`;

    return this._http.get(url)
      .map(x => x.json())
      .map(x => {
        let name = x.name.split(' ');
        return {
          firstName: name[0],
          lastName: name[1],
          email: x.email,
          provider: 'google'
        };
      });
  }
}
