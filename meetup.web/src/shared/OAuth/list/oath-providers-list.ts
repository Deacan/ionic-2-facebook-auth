import { OAuthProfileComponent } from './../profile/oauth-profile.component';

import { Component, OnInit } from '@angular/core';
import { OAuthService } from '../../services/index';
import { NavController } from 'ionic-angular';

@Component({
  templateUrl: './oath-providers-list.html'
})

export class OAuthProvidersListPage {
  constructor(private _oauthService: OAuthService, private _nav: NavController) { }

  public login(source: string) {
    this._oauthService.login(source).subscribe(() =>
      this._nav.setRoot(OAuthProfileComponent),
      // tslint:disable-next-line:align
      error => alert(error));
  }
}

