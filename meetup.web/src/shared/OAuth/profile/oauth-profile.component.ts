import { IOAuthProfile } from './../../interfaces/index';
import { OAuthService } from './../../services/index';
import { Component, OnInit } from '@angular/core';

@Component({
  templateUrl: './oauth-profile.component.html'
})

export class OAuthProfileComponent {


  private _profile: IOAuthProfile;

  public get profile(): IOAuthProfile {
    return this._profile;
  }

  public set profile(v: IOAuthProfile) {
    this._profile = v;
  }

  constructor(private _oathService: OAuthService) {
    _oathService.getProfile()
      .subscribe(profile => this.profile = profile);
  }
}
