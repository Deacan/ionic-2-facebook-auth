import { NgModule } from '@angular/core';

import {
  CallService,
  EmailService,
  InAppBrowserService,
  MapsService,
  OAuthService,
  FacebookOauthProvider,
  GoogleOAuthProvider
} from './index';

@NgModule({
  providers: [
    CallService,
    EmailService,
    InAppBrowserService,
    MapsService,
    OAuthService,
    FacebookOauthProvider,
    GoogleOAuthProvider

  ],
})
export class ServiceModule { }
